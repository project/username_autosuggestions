CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Credits
 * Maintainers


INTRODUCTION
------------
Suggests username if entered username exists.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/username_autosuggestions

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/username_autosuggestions

INSTALLATION
------------
Installing the module

Install as you would normally install a contributed drupal module.
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
-Enable module.
-Go to admin/config/people/accounts if you need to configure suggestion pattern.
-Override suggestion message template if needed (username_autosuggestions).

CREDITS
-------
The project sponsored by
Bright Solutions GmbH (https://www.drupal.org/node/1469032).

MAINTAINERS
-----------
Current maintainers:
 * Sergey Korzh (korgik) - https://drupal.org/user/813560
