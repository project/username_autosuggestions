<?php

/**
 * @file
 * Suggests username if entered username exists.
 */

/**
 * Default suggestions.
 */
define('USERNAME_AUTOSUGGESTIONS_DEFAULT', '[username][random-xx]
[email-firstpart]
[username][email-firstpart]
[email-firstpart][random-xx]');

/**
 * Implements hook_theme().
 */
function username_autosuggestions_theme() {
  return array(
    'username_autosuggestions' => array(
      'variables' => array('suggestions' => NULL),
    ),
  );
}

/**
 * Implements hook_form_alter().
 */
function username_autosuggestions_form_user_admin_settings_alter(&$form, &$form_state, $form_id) {
  $form['username_autosuggest'] = array(
    '#type' => 'fieldset',
    '#title' => t('Username auto suggestion'),
  );
  $form['username_autosuggest']['username_autosuggestions_suggestions'] = array(
    '#type' => 'textarea',
    '#title' => t('Suggestions'),
    '#default_value' => variable_get('username_autosuggestions_suggestions', USERNAME_AUTOSUGGESTIONS_DEFAULT),
    '#description' => t('Place suggestion rules, one per row. Tokens: [username], [email-firstpart], [random-x], [random-xx], [random-xxx], [random-xxxx], [field_custom_text_field] etc.'),
  );
  $form['username_autosuggest']['username_autosuggestions_max_allowed'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum allowed suggestions'),
    '#default_value' => variable_get('username_autosuggestions_max_allowed', 5),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('You may create big list of suggestions and limit output.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
}

/**
 * Implements hook_form_alter().
 */
function username_autosuggestions_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_register_form' || $form_id == 'user_profile_form') {
    $form['#validate'][] = 'username_autosuggestions_register_form_validate';
  }
}

/**
 * Validate form.
 */
function username_autosuggestions_register_form_validate($form, &$form_state) {
  if ($form['#user_category'] == 'account' || $form['#user_category'] == 'register') {
    $account = $form['#user'];
    $values = $form_state['values'];

    // Validate new or changing username.
    if (isset($values['name'])) {
      $name_exists = (bool) db_select('users')
        ->fields('users', array('uid'))
        ->condition('uid', $account->uid, '<>')
        ->condition('name', db_like($values['name']), 'LIKE')
        ->range(0, 1)
        ->execute()
        ->fetchField();

      if ($name_exists) {
        _username_autosuggestions_form_unset_error('name');
        $suggestions = _username_autosuggestions_get_suggestions($account, $values);
        $suggestions_output = theme('username_autosuggestions', array('suggestions' => $suggestions));
        form_set_error('name', t('The name %name is already taken. Try following: !suggestions',
            array('%name' => $values['name'], '!suggestions' => $suggestions_output)));
      }
    }
  }
}

/**
 * Returns list with replacements.
 */
function _username_autosuggestions_get_replacements($values) {
  $replacements = array(
    '[username]' => $values['name'],
    '[email-firstpart]' => strstr($values['mail'], '@', TRUE),
    '[random-x]' => rand(1, 9),
    '[random-xx]' => rand(10, 99),
    '[random-xxx]' => rand(100, 999),
    '[random-xxxx]' => rand(1000, 9999),
  );

  // Include only custom fields for security reasons.
  foreach ($values as $field_name => $custom_field_value) {
    if (substr($field_name, 0, 6) == 'field_') {
      $replacements['[' . $field_name . ']'] = !empty($custom_field_value[LANGUAGE_NONE][0]['value']) ? $custom_field_value[LANGUAGE_NONE][0]['value'] : '';
    }
  }

  return $replacements;
}

/**
 * Returns array of suggestions.
 */
function _username_autosuggestions_get_suggestions($account, $values) {
  $items = array();
  $max_allowed = variable_get('username_autosuggestions_max_allowed', 5);
  $suggestions_rules = variable_get('username_autosuggestions_suggestions', USERNAME_AUTOSUGGESTIONS_DEFAULT);

  $replacements = _username_autosuggestions_get_replacements($values);
  $suggestions_rules = str_replace(array_keys($replacements), $replacements, $suggestions_rules);
  if (!empty($suggestions_rules)) {
    $suggestions = explode("\n", $suggestions_rules);

    // Clear replaced values.
    foreach ($suggestions as $value) {
      $value = check_plain($value);
      $value = trim($value);
      $value = substr($value, 0, 60);
      if (!empty($value) && !is_numeric($value) && $value != $values['name'] && !in_array($value, $items)) {
        $items[] = $value;
      }
    }

    // Exclude exists users.
    if (!empty($items)) {
      $names_exist = db_select('users')
        ->fields('users', array('name'))
        ->condition('uid', $account->uid, '<>')
        ->condition('name', $items, 'IN')
        ->execute()
        ->fetchCol();
      $items = array_udiff($items, $names_exist, 'strcasecmp');
    }

    if (!empty($items)) {
      $items = array_slice($items, 0, $max_allowed);
    }
  }

  return $items;
}

/**
 * Clears an error against one form element.
 *
 * @param string $name
 *   The name of the form element.
 */
function _username_autosuggestions_form_unset_error($name) {
  $errors = &drupal_static('form_set_error', array());
  $removed_messages = array();
  if (isset($errors[$name])) {
    $removed_messages[] = $errors[$name];
    unset($errors[$name]);
  }
  $_SESSION['messages']['error'] = array_diff($_SESSION['messages']['error'], $removed_messages);
  if (empty($_SESSION['messages']['error'])) {
    unset($_SESSION['messages']['error']);
  }
}

/**
 * Returns HTML for username autosuggestions.
 */
function theme_username_autosuggestions($variables) {
  if (!empty($variables['suggestions'])) {
    return theme('item_list', array('items' => $variables['suggestions']));
  }
  return '';
}
